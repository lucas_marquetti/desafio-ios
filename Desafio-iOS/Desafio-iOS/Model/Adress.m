//
//  Adress.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "Adress.h"


@implementation Adress

//@synthesize cep,city,district,state,street,type;


//+ (NSDictionary *)JSONKeyPathsByPropertyKey {
//    return @{
//             @"cep": @"cep",
//             @"district": @"bairro",
//             @"state": @"estado",
//             @"street": @"logradouro",
//             @"type": @"tipodelogradouro",
//             @"city": @"cidade",
//             };
//}


#pragma mark - init the self
-(id) initWithDictionary : (NSDictionary*) dict{
    
    _cep = dict[@"cep"];
    _bairro = dict[@"bairro"];
    _estado = dict[@"estado"];
    _logradouro = dict[@"logradouro"];
    _tipoDeLogradouro = dict[@"tipoDeLogradouro"];
    _cidade = dict[@"cidade"];
    
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    
    return self;
}

@end

