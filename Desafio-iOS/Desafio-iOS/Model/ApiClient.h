//
//  ApiClient.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Adress.h"
@protocol ApiClientDelegate <NSObject>

-(void)ApiClientDelegateDidFinish:(Adress*)result;
-(void)ApiClientDelegateFailWithError:(NSString*)error;

@end

@interface ApiClient : NSObject

+ (void)searchCep: (NSString*)cep delegate:(id<ApiClientDelegate>)delegate;

@end
