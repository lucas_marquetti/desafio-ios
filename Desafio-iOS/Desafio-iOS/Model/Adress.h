//
//  Adress.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
@interface Adress : MTLModel<MTLJSONSerializing>

#pragma mark - propertys

@property (nonatomic, strong) NSString* cidade;
@property (nonatomic, strong) NSString* estado;
@property (nonatomic, strong) NSString* bairro;
@property (nonatomic, strong) NSString* cep;
@property (nonatomic, strong) NSString* tipoDeLogradouro;
@property (nonatomic, strong) NSString* logradouro;


#pragma mark - methods

+ (NSDictionary *)JSONKeyPathsByPropertyKey;
-(id) initWithDictionary : (NSDictionary*) dict;
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error;

@end
