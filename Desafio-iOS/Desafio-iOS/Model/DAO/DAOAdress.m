//
//  DAOAdress.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "DAOAdress.h"
#import "DAOFolders.h"
#import "FMDatabase.h"

@implementation DAOAdress

#pragma mark - Private Methods
-(FMDatabase*)getDatabase{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *dbPath = [[DAOFolders storagePath] stringByAppendingPathComponent:@"DB/Desafio.sqlite"];

	BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    FMDatabase* database = nil;
    if (success) {
        database = [FMDatabase databaseWithPath:dbPath];
        if ([database open]) {
            return database;
        }
    }
    return nil;
}


- (void)insertAdress:(Adress *)adress
{
    FMDatabase* db = [self getDatabase];
    
    NSString *query = [NSString stringWithFormat:@"insert into ADRESS values (%@, '%@', '%@', '%@', '%@','%@')",
                       adress.cep, adress.logradouro, adress.bairro, adress.estado,
                       adress.cidade, adress.tipoDeLogradouro];
    
    [db executeUpdate:query];
    
    if (db.lastError.code!=0) {
        NSLog(@"%@",db.lastError.description);
    }
    [db close];
    
    
}


- (Adress *)getAdressWithCep:(NSString *)cep
{
    //[NSString stringWithFormat:@"Select cd_conta, cd_ddd, nr_telefone, nm_empresa from CONTA where cep = %@", cep]
    
    NSString *preQuery = [NSString stringWithFormat:@"SELECT * FROM ADRESS WHERE cep = %@", cep];//WHERE cep %@//, cep
    
    FMDatabase* db = [self getDatabase];
    FMResultSet* results = [db executeQuery:preQuery];
    
    Adress *adressNew;
    
    while ([results next]) {
        
        adressNew = [[Adress alloc]initWithDictionary:[results resultDictionary] error:Nil];
       
    }
    [db close];
    return (adressNew) ? adressNew : nil;
    

}

- (NSMutableArray *)getArrayAdressWithCep
{
    NSMutableArray * arrayAdress = [[NSMutableArray alloc]init];
    
    NSString *preQuery = [NSString stringWithFormat:@"SELECT * FROM ADRESS"];//WHERE cep %@//, cep
    
    FMDatabase* db = [self getDatabase];
    FMResultSet* results = [db executeQuery:preQuery];
    
    Adress *adressNew;
    
    while ([results next]) {
        
        adressNew = [[Adress alloc]initWithDictionary:[results resultDictionary] error:Nil];
        [arrayAdress addObject:adressNew];
    }
    [db close];
    
    return arrayAdress;
    
}

@end
