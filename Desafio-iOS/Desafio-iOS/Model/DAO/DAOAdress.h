//
//  DAOAdress.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Adress.h"

@interface DAOAdress : NSObject

- (void)insertAdress:(Adress *)adress;
- (Adress *)getAdressWithCep:(NSString *)cep;
- (NSMutableArray *)getArrayAdressWithCep;

@end
