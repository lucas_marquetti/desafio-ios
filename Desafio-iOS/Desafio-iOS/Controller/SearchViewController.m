//
//  ViewController.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 04/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "SearchViewController.h"
#import "ApiClient.h"
#import "Adress.h"
#import "OlderResultViewController.h"
#import "MaskedTextField.h"
#import "DAOAdress.h"
#import "DAOFolders.h"

@interface SearchViewController ()<ApiClientDelegate,UIAlertViewDelegate,UITextFieldDelegate>{
    Adress* _adress;
    NSMutableArray * _arrayOlders;
}
@property (strong, nonatomic) MaskedTextField* mask;

@end

@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _arrayOlders = [[NSMutableArray alloc]init];
    _txtCep.delegate = self.mask;
    self.mask.textFieldDelegate=self;
    
    UITapGestureRecognizer * tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hiddenKey)];
    [self.view addGestureRecognizer:tapgesture];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - maskCep
- (MaskedTextField*)mask
{
    if (!_mask) {
        MaskFormatter *cnpjFormatter = [[MaskFormatter alloc] initWithMaskList:[NSArray arrayWithObjects:@"___.__-___", nil]];
        _mask = [[MaskedTextField alloc] initWithFormatter:cnpjFormatter];
    }
    return _mask;
}

#pragma mark - IBActions
- (IBAction)newSearch:(id)sender {
    
    NSData *asciiEncoded = [_txtCep.text dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *cep = [[NSString alloc] initWithData:asciiEncoded encoding:NSASCIIStringEncoding];
    cep = [cep stringByReplacingOccurrencesOfString:@"." withString:@""];
    cep = [cep stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    if ([cep length]==8) {
        //Verifica se o cep ja foi pesquisado. Fazer a persistencia dos dados
        if ([self consultArray:cep]) {
            [ApiClient searchCep:cep delegate:self];
        }else{
            [self insertValues];
        }
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Erro" message:@"O cep deve ter oito dígitos" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
}

- (IBAction)olderSearch:(id)sender {
    
    [self performSegueWithIdentifier:@"oldSearchCep" sender:self];
}


#pragma mark - Verify Array
-(BOOL) addObjectPermission{
    BOOL perm = YES;
    
    for (Adress* adressSearch in _arrayOlders) {
        if ([adressSearch.cep isEqualToString:_adress.cep]) {
            perm = NO;
        }
    }
    return perm;
}

-(BOOL) consultArray:(NSString*) cep{
    BOOL perm = YES;
    
    DAOAdress * op = [DAOAdress new];
    Adress *  arrayDB = [op getAdressWithCep:cep];
    if (arrayDB) {
        perm = NO;
        _adress = arrayDB;
    }

    return perm;
}


#pragma mark - HiddenKeyBoard
-(void) hiddenKey{
    [self.view endEditing:YES];
}

#pragma mark - InsertValueLabels
-(void) insertValues{
    
    _lblCep.text = _adress.cep;
    _lblStreet.text = _adress.logradouro;
    _lblDistrict.text = _adress.bairro;
    _lblState.text = _adress.estado;
    _lblCity.text = _adress.cidade;
}
#pragma mark - ApiDelegate
-(void)ApiClientDelegateDidFinish:(Adress*)result{
    _adress = result;
    
    [self insertValues];
    
    if ([self addObjectPermission]) {
        DAOAdress * op = [DAOAdress new];
        [op insertAdress:_adress];
        [_arrayOlders  addObject:_adress];
    }
}

-(void) ApiClientDelegateFailWithError:(NSString *)error{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Erro" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}
@end
