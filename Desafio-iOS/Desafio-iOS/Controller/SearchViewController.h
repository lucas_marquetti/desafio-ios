//
//  ViewController.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 04/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController

#pragma mark - propertys
@property (nonatomic, strong) IBOutlet UITextField *txtCep;
@property (strong, nonatomic) IBOutlet UILabel *lblCep;
@property (strong, nonatomic) IBOutlet UILabel *lblStreet;
@property (strong, nonatomic) IBOutlet UILabel *lblDistrict;
@property (strong, nonatomic) IBOutlet UILabel *lblCity;
@property (strong, nonatomic) IBOutlet UILabel *lblState;


#pragma mark - IBActions
- (IBAction)newSearch:(id)sender;
- (IBAction)olderSearch:(id)sender;


@end
