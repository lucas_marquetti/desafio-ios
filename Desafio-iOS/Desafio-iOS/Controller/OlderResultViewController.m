//
//  OlderResultViewController.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "OlderResultViewController.h"
#import "AdressCell.h"
#import "Adress.h"
#import "DAOAdress.h"

@interface OlderResultViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation OlderResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    DAOAdress * dao = [ DAOAdress new];
    _arrayOldersSearchs = [dao getArrayAdressWithCep];
    [_tableView reloadData];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_arrayOldersSearchs count];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"AdressCellIdentifier";
    
    AdressCell *cell = (AdressCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AdressCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.backgroundColor = [UIColor clearColor];
    [cell setAdress:_arrayOldersSearchs[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}



@end
