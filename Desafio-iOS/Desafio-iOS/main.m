//
//  main.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 04/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
