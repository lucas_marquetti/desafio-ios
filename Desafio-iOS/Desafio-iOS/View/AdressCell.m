//
//  AdressCell.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "AdressCell.h"

@implementation AdressCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setAdress:(Adress*) adress{
    _cep.text = adress.cep;
    _lblCity.text = adress.cidade;
    _lblDistrict.text = adress.bairro;
    _lblState.text = adress.estado;
    _lblStreet.text = adress.logradouro;
}

@end
