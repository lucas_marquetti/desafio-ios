//
//  CepTestCase.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Adress.h"
#import "ApiClient.h"

@interface CepTestCase : XCTestCase < ApiClientDelegate>

@end

@implementation CepTestCase

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}


- (void)testCEPAdress
{
    [ApiClient searchCep:@"04601061" delegate:self];
  
}

-(void) ApiClientDelegateDidFinish:(Adress *)result{
    
    XCTAssertEqual(result, 1, @"Should have matched");
}

@end
